package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;
import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.util.List;

public class CompositeMain {
    public static void main(String[] args){
        Company company = new Company();

        Employees naruto = new Ceo("Naruto", 500000.00);
        company.addEmployee(naruto);

        Employees sasuke = new Cto("Sasuke", 320000.00);
        company.addEmployee(sasuke);

        Employees kakashi = new BackendProgrammer("Kakashi", 200000.00);
        company.addEmployee(kakashi);

        Employees sakura = new FrontendProgrammer("Sakura", 130000.00);
        company.addEmployee(sakura);

        Employees hinata = new UiUxDesigner("Hinata", 177000.00);
        company.addEmployee(hinata);

        Employees shikamaru = new NetworkExpert("Shikamaru", 83000.00);
        company.addEmployee(shikamaru);

        Employees gaara = new SecurityExpert("Gaara", 70000.00);
        company.addEmployee(gaara);

        List<Employees> allEmployees = company.getAllEmployees();
        System.out.println("Employees : ");
        for (int index = 0; index < allEmployees.size(); index++) {
            System.out.println(allEmployees.get(index));
        }

        System.out.println("Total salary : " + company.getNetSalaries());

    }
}
