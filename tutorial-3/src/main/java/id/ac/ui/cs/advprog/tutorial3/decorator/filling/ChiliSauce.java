package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        this.description = food.getDescription() + ", adding chili sauce";
        return description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.3;
    }
}
