package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        this.description = food.getDescription() + ", adding chicken meat";
        return description;
    }

    @Override
    public double cost() {
        return food.cost() + 4.50;
    }
}
