package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
      if(salary < 30000.00){
        throw new IllegalArgumentException("Front End Programmer Salary must not lower than 30000.00");
      }else{
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
      }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
