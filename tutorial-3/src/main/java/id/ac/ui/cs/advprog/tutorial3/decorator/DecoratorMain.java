package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class DecoratorMain {
    static Food thickBunBurgerSpecial; // thick with beef, cheese, cucumber, lettuce, chili sauce
    static Food thinBunBurgerVegetarian;
    static Food doubleBeefChickenDoubleSauceSandwich;
    static Food noCrustAllFillingSandwich;

    public static void main(String[] args){

        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println("====================================================");
        System.out.println(thickBunBurgerSpecial.getDescription());
        System.out.println("Cost : " + thickBunBurgerSpecial.cost());

        thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println("====================================================");
        System.out.println(thinBunBurgerVegetarian.getDescription());
        System.out.println("Cost : " + thinBunBurgerVegetarian.cost());

        doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println("====================================================");
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());
        System.out.println("Cost : " + doubleBeefChickenDoubleSauceSandwich.cost());

        noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();
        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);
        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        System.out.println("====================================================");
        System.out.println(noCrustAllFillingSandwich.getDescription());
        System.out.println("Cost : " + noCrustAllFillingSandwich.cost());
    }
}
