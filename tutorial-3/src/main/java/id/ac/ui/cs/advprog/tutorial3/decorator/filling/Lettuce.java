package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        this.description = food.getDescription() + ", adding lettuce";
        return description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.75;
    }
}
