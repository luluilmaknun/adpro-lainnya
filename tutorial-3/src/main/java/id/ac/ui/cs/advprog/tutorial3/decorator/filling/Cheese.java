package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        this.description = food.getDescription() + ", adding cheese";
        return description;
    }

    @Override
    public double cost() {
        return food.cost() + 2.00;
    }
}
